﻿using System;
using System.IO;
using LumenWorks.Framework.IO.Csv;
using MySql.Data.MySqlClient;
using System.Text;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading;
namespace yoshimicsvextract
{
    class Program
    {
    static string cur_date = DateTime.Now.ToString("yyyy-MM-dd");
    private static readonly HttpClient client = new HttpClient();
    public static string oDate;
    public static string MachName;
    public static string ProgName;
    public static string oProcstarttime;
    public static string oProcendtime;
    public static TimeSpan ProcTime;
    public static string oMachOprTime;
    public static string oLaserProcTime;
    public static string oNCTProcTime;
    public static TimeSpan NCTProcTime;
    public static string oAlarmTime;
    public static TimeSpan AlarmTime;
    public static string MatName;
    public static string oThickness;
    public static string oSheetX;
    public static string oSheetY;
    public static string oYieldRate;
    public static string oGood;
    public static string oNGQty;
    public static string Speed;
    static void Main(string[] args)
    {
        string connstring = "Server = localhost; Database = factory; Uid = root; Pwd = ;SslMode=none";
        MySqlConnection mcon = new MySqlConnection(connstring);
        mcon.Open();
        string connstring_setupdb = "Server = localhost; Database = setupsheetdb; Uid = root; Pwd = ;SslMode=none";
        MySqlConnection mcon_setup = new MySqlConnection(connstring_setupdb);
        mcon_setup.Open();
        try
        {

            string CmdText1 = "DELETE FROM yoshimi_csvquickdata WHERE !(procend like '" + cur_date + "%')";
            using (MySqlCommand cmd1 = new MySqlCommand(CmdText1, mcon))
            {

                cmd1.ExecuteNonQuery();
            }


            var files = Directory.GetFiles(@"F:\xampp\htdocs\alfadockpro\factory_layout\131\RPA", "*.csv");

            foreach (String f in files)
            {
                string fname = System.IO.Path.GetFileNameWithoutExtension(f);
                if (f.Contains("quick_view_data"))
                {
                    extract_quickview_info(f, mcon, mcon_setup);
                }
                else if (f.Contains("OEEData"))
                {
                    extract_oeee_info(f, mcon);
                }
                else if (f.Contains("DailyProductInfo"))
                {
                    extract_product_info(f, mcon);
                }
                else if (f.Contains("Bending-ProcessingData"))
                {
                    extract_bend_processdata(f, mcon);
                }
                else if (f.Contains("Blanking-ProcessingData"))
                {
                    extract_blank_processdata(f, mcon, mcon_setup);
                }
                

            }
        }
        catch (Exception e)
        {
            Console.WriteLine(e.StackTrace);
            using (StreamWriter w = File.AppendText("log.txt"))
            {
                Log("ERROr: " + e.StackTrace, w);
            }
        }
        finally
        {
            if (mcon != null)
            {
                mcon.Close();
            }

        }
    }
    public static void updateYOSHIMIpgminfo(string partno, string sdate, string edate, MySqlConnection mcon)
    {
           
            try
        {
            Program pg = new Program();
            sdate = sdate.Replace("/", "-");
            edate = edate.Replace("/", "-");
            string query = string.Format(@"UPDATE `setupsheetdb`.setupsheetyoshimi SET processed=2,starttime='{1}',endtime='{2}'  where compid='131' AND programname like '{0}%' AND (starttime='0000-00-00 00:00:00' OR endtime='0000-00-00 00:00:00') AND processed!=2", partno, sdate, edate);

            using (MySqlCommand command = new MySqlCommand(query, mcon))
            {
                int row = command.ExecuteNonQuery();
                    if (row == 1)
                    {

                        using (StreamWriter w = File.AppendText("log.txt"))
                        {
                            Log("Success: " + partno, w);
                        }
                    }
            }


        }
        catch (Exception)
        {
        }
        finally
        {

        }


    }
    public static void extract_bend_processdata(string path, MySqlConnection mcon)
    {

        using (CsvReader csv =
                    new CsvReader(new StreamReader(path), true))
        {

            int counter = 1;
            int fieldCount = csv.FieldCount;
            string[] headers = csv.GetFieldHeaders();
            while (csv.ReadNextRecord())
            {
               
                // Date
                oDate = csv[0];
                DateTime Date = DateTime.Parse(oDate);

                //Machine Name
                MachName = csv[1];
                MachName = MachName.Replace("Ⅱ/ASR2512N/", "2");
                MachName = MachName.Replace(" M‡U / ASR2512N /", "M2");
                MachName = MachName.Replace("/ AS2512NTK.ULS2512NTK /", (MachName.Contains("ACIES-2512TAJ")) ? "" : " ");
                MachName = MachName.Replace("/AS2512NTK.ULS2512NTK/", (MachName.Contains("ACIES-2512TAJ")) ? "" : " ");
                MachName = MachName.Replace("/ AS48RM.ULS48RM /", "");
                MachName = MachName.Replace("/AS48RM.ULS48RM/", "");
                MachName = MachName.Replace("/ AS3015F1 /", "");
                MachName = MachName.Replace("/ ASR3015NTK /", "");
                MachName = MachName.Replace("ASR3015NTK", "");
                MachName = MachName.Replace("/ ASR48M /", "");
                MachName = MachName.Replace("ASR48M", "");
                MachName = MachName.Replace("AS3015F1", "");
                MachName = MachName.Replace("/", "");
                MachName = MachName.Replace("Ⅲ-", "3-");
                MachName = MachName.Replace("‡V-", "3-");
                //Program Name
                ProgName = csv[2];
                DateTime Procstarttime; DateTime Procendtime; double ProcTimeSecs; double OprTimeSecs;
                double AlarmTimeSecs; TimeSpan MachOprTime; float YieldRate;
                //Process start time

                oProcstarttime = csv[4];
                Procstarttime = DateTime.Parse(oProcstarttime);


                //Process end time
                oProcendtime = csv[5];
                Procendtime = DateTime.Parse(oProcendtime);

                //Processing Time
                ProcTime = Procendtime - Procstarttime;
                ProcTimeSecs = ProcTime.TotalSeconds;

                //Machine Operation Time
                oMachOprTime = csv[6].Replace("-", "");
                MachOprTime = TimeSpan.Parse(oMachOprTime);
                OprTimeSecs = MachOprTime.TotalSeconds;


                //Alarm Time
                oAlarmTime = csv[7];
                if (oAlarmTime.Contains("-"))
                {
                    oAlarmTime = "00:00:00";
                }
                AlarmTime = TimeSpan.Parse(oAlarmTime);
                AlarmTimeSecs = AlarmTime.TotalSeconds;

                //Material name
                MatName = csv[8];


                //Thickness
                oThickness = csv[9];
                float Thickness = float.Parse(oThickness);

                //SheetX
                oSheetX = csv[10];
                oSheetX = oSheetX.Remove(oSheetX.Length - 3);
                int SheetX = Int32.Parse(oSheetX);

                //SheetY
                oSheetY = csv[11];
                oSheetY = oSheetY.Remove(oSheetY.Length - 3);
                int SheetY = Int32.Parse(oSheetY);

                //Yield rate
                oYieldRate = csv[12];
                YieldRate = float.Parse(oYieldRate);

                //Good
                oGood = csv[14];
                //bool Good = bool.Parse(oGood);
                //bool Good = Convert.ToBoolean(oGood);

                //NG Qty
                oNGQty = csv[15];
                //bool NGQty = bool.Parse(oNGQty);

                //Speed
                Speed = csv[13];

                string CmdText = "INSERT IGNORE INTO yoshimi_csvextract VALUES(@idcsv,@date,@machname,@progname,@procstrtime,@procendtime,@proctime,@machoprtime,@laserproctime,@nctproctime,@alarmtime,@matname,@thickness,@sheetx,@sheety,@yieldrate,@good,@ngqty,@speed,@proctimesecs,@alarmtimesecs,@oprtimesecs)";
                MySqlCommand cmd = new MySqlCommand(CmdText, mcon);

                cmd.Parameters.AddWithValue("@idcsv", oDate + "_" + "be" + counter);
                cmd.Parameters.AddWithValue("@date", Date);
                cmd.Parameters.AddWithValue("@machname", Encoding.UTF8.GetBytes(MachName));
                cmd.Parameters.AddWithValue("@progname", ProgName);
                cmd.Parameters.AddWithValue("@procstrtime", Procstarttime);
                cmd.Parameters.AddWithValue("@procendtime", Procendtime);
                cmd.Parameters.AddWithValue("@proctime", ProcTime);
                cmd.Parameters.AddWithValue("@machoprtime", MachOprTime);
                cmd.Parameters.AddWithValue("@laserproctime", "00:00:00");
                cmd.Parameters.AddWithValue("@nctproctime", NCTProcTime);
                cmd.Parameters.AddWithValue("@alarmtime", AlarmTime);
                cmd.Parameters.AddWithValue("@matname", MatName);
                cmd.Parameters.AddWithValue("@thickness", 0);
                cmd.Parameters.AddWithValue("@sheetx", oSheetX);
                cmd.Parameters.AddWithValue("@sheety", oSheetY);
                cmd.Parameters.AddWithValue("@yieldrate", YieldRate);
                cmd.Parameters.AddWithValue("@good", oGood);
                cmd.Parameters.AddWithValue("@ngqty", oNGQty);
                cmd.Parameters.AddWithValue("@speed", Speed);
                cmd.Parameters.AddWithValue("@proctimesecs", ProcTimeSecs);
                cmd.Parameters.AddWithValue("@alarmtimesecs", AlarmTimeSecs);

                cmd.Parameters.AddWithValue("@oprtimesecs", OprTimeSecs);

                cmd.ExecuteNonQuery();
                counter++;
            }
        }
    }
    public static void extract_blank_processdata(string path, MySqlConnection mcon, MySqlConnection mcon2)
    {

        using (CsvReader csv =
                    new CsvReader(new StreamReader(path), true))
        {

            int counter = 1;
            int fieldCount = csv.FieldCount;
            string[] headers = csv.GetFieldHeaders();
            while (csv.ReadNextRecord())
            {
                //for (int i = 0; i < fieldCount; i++)
                //Console.Write(string.Format("{0} = {1};",headers[i], csv[i]));

                // Date
                oDate = csv[1];
                DateTime Date = DateTime.Parse(oDate);

                //Machine Name
                MachName = csv[2];

                MachName = MachName.Replace("/AS2512NTK.ULS2512NTK/63511098", "");
                MachName = MachName.Replace("/AS48RM.ULS48RM/43513019", "");
                
                //Program Name
                ProgName = csv[3];

                //Process start time
                oProcstarttime = csv[8];
                DateTime Procstarttime = DateTime.Parse(oProcstarttime);


                //Process end time
                oProcendtime = csv[9];
                DateTime Procendtime = DateTime.Parse(oProcendtime);

                //Processing Time
                ProcTime = Procendtime - Procstarttime;
                double ProcTimeSecs = ProcTime.TotalSeconds;

                //Machine Operation Time
                oMachOprTime = csv[10];
                TimeSpan MachOprTime = TimeSpan.Parse(oMachOprTime);
                double OprTimeSecs = MachOprTime.TotalSeconds;

                //Laser processing time
                oLaserProcTime = csv[11];
                TimeSpan LaserProcTime = TimeSpan.Parse(oLaserProcTime);

                //NCT processing time
                oNCTProcTime = csv[12];
                if (oNCTProcTime.Contains("-"))
                {
                    oNCTProcTime = "00:00:00";
                }
                NCTProcTime = TimeSpan.Parse(oNCTProcTime);

                //Alarm Time
                oAlarmTime = csv[13];
                if (oAlarmTime.Contains("-"))
                {
                    oAlarmTime = "00:00:00";
                }
                AlarmTime = TimeSpan.Parse(oAlarmTime);
                double AlarmTimeSecs = AlarmTime.TotalSeconds;

                //Material name
                MatName = csv[14];


                //Thickness
                oThickness = csv[15];
                float Thickness = float.Parse(oThickness);

                //SheetX
                oSheetX = csv[16];
                oSheetX = oSheetX.Remove(oSheetX.Length - 3);
                int SheetX = Int32.Parse(oSheetX);

                //SheetY
                oSheetY = csv[17];
                oSheetY = oSheetY.Remove(oSheetY.Length - 3);
                int SheetY = Int32.Parse(oSheetY);

                //Yield rate
                oYieldRate = csv[18];
                float YieldRate = float.Parse(oYieldRate);

                //Good
                oGood = csv[19];
                //bool Good = bool.Parse(oGood);
                //bool Good = Convert.ToBoolean(oGood);

                //NG Qty
                oNGQty = csv[20];
                //bool NGQty = bool.Parse(oNGQty);

                //Speed
                Speed = csv[21];


                string CmdText = "INSERT IGNORE INTO yoshimi_csvextract VALUES(@idcsv,@date,@machname,@progname,@procstrtime,@procendtime,@proctime,@machoprtime,@laserproctime,@nctproctime,@alarmtime,@matname,@thickness,@sheetx,@sheety,@yieldrate,@good,@ngqty,@speed,@proctimesecs,@alarmtimesecs,@oprtimesecs)";
                MySqlCommand cmd = new MySqlCommand(CmdText, mcon);

                cmd.Parameters.AddWithValue("@idcsv", oProcstarttime + "_" + "blank");
                cmd.Parameters.AddWithValue("@date", Date);
                cmd.Parameters.AddWithValue("@machname", Encoding.UTF8.GetBytes(MachName));
                cmd.Parameters.AddWithValue("@progname", ProgName);
                cmd.Parameters.AddWithValue("@procstrtime", Procstarttime);
                cmd.Parameters.AddWithValue("@procendtime", Procendtime);
                cmd.Parameters.AddWithValue("@proctime", ProcTime);
                cmd.Parameters.AddWithValue("@machoprtime", MachOprTime);
                cmd.Parameters.AddWithValue("@laserproctime", "00:00:00");
                cmd.Parameters.AddWithValue("@nctproctime", NCTProcTime);
                cmd.Parameters.AddWithValue("@alarmtime", AlarmTime);
                cmd.Parameters.AddWithValue("@matname", MatName);
                cmd.Parameters.AddWithValue("@thickness", 0);
                cmd.Parameters.AddWithValue("@sheetx", 0);
                cmd.Parameters.AddWithValue("@sheety", 0);
                cmd.Parameters.AddWithValue("@yieldrate", 0);
                cmd.Parameters.AddWithValue("@good", oGood);
                cmd.Parameters.AddWithValue("@ngqty", oNGQty);
                cmd.Parameters.AddWithValue("@speed", Speed);
                cmd.Parameters.AddWithValue("@proctimesecs", ProcTimeSecs);
                cmd.Parameters.AddWithValue("@alarmtimesecs", AlarmTimeSecs);

                cmd.Parameters.AddWithValue("@oprtimesecs", OprTimeSecs);

                int result = cmd.ExecuteNonQuery();
                  /* if (result != 1)
                    {
                        if (oNGQty.Equals("0"))
                        {
                            updateYOSHIMIpgminfo(ProgName, oProcstarttime, oProcendtime, mcon2);
                        }
                    }*/
                counter++;
            }
        }
    }
    
    
    public static void extract_product_info(string path, MySqlConnection mcon)
    {
        string val_id = "";
        if (path.Contains("Bending"))
        {
            val_id = "Bending";
        }
        else if (path.Contains("Blanking"))
        {
            val_id = "Blanking";
        }
        string MachName;
        string str_Date;
        string Str_ActProcCount;
        string Str_TargetCount;
        using (CsvReader csv = new CsvReader(new StreamReader(path), true))
        {
            int counter = 1;
            int fieldCount = csv.FieldCount;
            string[] headers = csv.GetFieldHeaders();
            while (csv.ReadNextRecord())
            {
                //for (int i = 0; i < fieldCount; i++)
                //Console.Write(string.Format("{0} = {1};",headers[i], csv[i]));


                //Machine Name
                MachName = csv[0];
                MachName = MachName.Replace("/AS2512NTK.ULS2512NTK/63511098", "");
                MachName = MachName.Replace("/AS48RM.ULS48RM/43513019", "");
                

                //Actual Process Count
                Str_ActProcCount = csv[4];

                //Target Process Count
                Str_TargetCount = csv[5];

                //Date
                str_Date = csv[1] + "/" + csv[2] + "/" + csv[3];
                DateTime Date = DateTime.Parse(str_Date);

                string str_Date_1 = csv[1] + "-" + csv[2] + "-" + csv[3];

                string CmdText = "INSERT IGNORE INTO yoshimi_csvprocess VALUES(@idcsv,@machname,@date,@actproccount,@targetcount,@machtype)";
                MySqlCommand cmd = new MySqlCommand(CmdText, mcon);
                cmd.Parameters.AddWithValue("@idcsv", str_Date_1 + "_" + MachName);
                cmd.Parameters.AddWithValue("@machname", Encoding.UTF8.GetBytes(MachName));
                cmd.Parameters.AddWithValue("@date", Date);
                cmd.Parameters.AddWithValue("@actproccount", Str_ActProcCount);
                cmd.Parameters.AddWithValue("@targetcount", Str_TargetCount);
                cmd.Parameters.AddWithValue("@machtype", val_id);


                int result = cmd.ExecuteNonQuery();
                if (result != 1)
                {
                    string query = string.Format(@"UPDATE yoshimi_csvprocess SET actproccount={1}, targetcount={2} where NOT actproccount ={1} AND idcsv='{0}'", str_Date_1 + "_" + MachName , Str_ActProcCount, Str_TargetCount);


                    using (MySqlCommand command = new MySqlCommand(query, mcon))
                    {
                        int row = command.ExecuteNonQuery();
                    }
                }
                counter++;
            }

        }
    }
    public static void extract_oeee_info(string path, MySqlConnection mcon)
    {
        try
        {
            string val_id = "";
            if (path.Contains("Bending"))
            {
                val_id = "Bending";
            }
            else if (path.Contains("Blanking"))
            {
                val_id = "Blanking";
            }

            string MachName;

            string date;
            float optime;
            float settime;
            float idletime;
            float errtime;
            using (CsvReader csv =
             new CsvReader(new StreamReader(path), true))
            {

                int counter = 1;
                int fieldCount = csv.FieldCount;
                string[] headers = csv.GetFieldHeaders();
                while (csv.ReadNextRecord())
                {
                    //for (int i = 0; i < fieldCount; i++)
                    //Console.Write(string.Format("{0} = {1};",headers[i], csv[i]));


                    //Machine Name
                    MachName = csv[0];
                    MachName = MachName.Replace("/AS2512NTK.ULS2512NTK/63511098", "");
                    MachName = MachName.Replace("/AS48RM.ULS48RM/43513019", "");
                   

                    //Date
                    date = csv[1];


                    //time
                    optime = float.Parse(csv[2]);
                    settime = float.Parse(csv[3]);
                    idletime = float.Parse(csv[4]);
                    errtime = float.Parse(csv[5]);

                    float Totaltime = optime + settime + idletime + errtime;
                    float op_percent = optime;
                    float set_percent = settime;
                    float idle_percent = idletime;
                    float err_percent = errtime;
                    /*  if (Totaltime != 0)
                      {

                           op_percent = (optime * 100) / Totaltime;
                          set_percent = (settime * 100) / Totaltime;
                           idle_percent = (idletime * 100) / Totaltime;
                           err_percent = (errtime * 100) / Totaltime;
                      }*/

                    string CmdText = "INSERT IGNORE INTO yoshimi_csvtime VALUES(@idcsv,@machname,@date,@optime,@settime,@idletime,@errtime,@tottime,@avgvalue,@machtype)";
                    MySqlCommand cmd = new MySqlCommand(CmdText, mcon);
                    cmd.Parameters.AddWithValue("@idcsv", date + "_" + MachName);
                    cmd.Parameters.AddWithValue("@machname", Encoding.UTF8.GetBytes(MachName));
                    cmd.Parameters.AddWithValue("@date", date);
                    cmd.Parameters.AddWithValue("@optime", op_percent);
                    cmd.Parameters.AddWithValue("@settime", set_percent);
                    cmd.Parameters.AddWithValue("@idletime", idle_percent);
                    cmd.Parameters.AddWithValue("@errtime", err_percent);
                    cmd.Parameters.AddWithValue("@tottime", Totaltime);
                    cmd.Parameters.AddWithValue("@avgvalue", 100);
                    cmd.Parameters.AddWithValue("@machtype", val_id);

                    int result = cmd.ExecuteNonQuery();

                    if (result != 1)
                    {
                        string query = string.Format(@"UPDATE yoshimi_csvtime SET optime={1} , settime={2}, idletime={3}, errtime={4}, tottime={5} where NOT tottime ={5} AND idcsv='{0}'", date + "_" + MachName, op_percent, set_percent, idle_percent, err_percent, Totaltime);


                        using (MySqlCommand command = new MySqlCommand(query, mcon))
                        {
                            int row = command.ExecuteNonQuery();
                        }
                    }
                    counter++;

                }
            }
        }
        catch (Exception e)
        {
            using (StreamWriter w = File.AppendText("log.txt"))
            {
                Log("Error:" + e, w);
            }
        }
    }
    public static void extract_quickview_info(string path, MySqlConnection mcon, MySqlConnection mcon2)
    {

        string fileName;
        string Act_fileName;
        string MachName = "";
        string ProgNo;
        string procstrt;
        string procend;
        string goodqty;
        string defqty;
        string bendsteps;
        string shots;
        TimeSpan ProcTime;
        try
        {
            using (CsvReader csv = new CsvReader(new StreamReader(path), true))
            {
                string starttime = "";
                string endtime = "";
                string ProgNo_old = "";
                int good_qty_val = 0;
                int counter = 1;
                fileName = Path.GetFileNameWithoutExtension(path);
                Act_fileName = fileName.Replace("-quick_view_data", "");

                //int fieldCount = csv.FieldCount;
                string[] headers = csv.GetFieldHeaders();
                foreach (string headval in headers)
                {
                    if (headval.Contains("アラームメッセージ"))
                        return;
                }
                while (csv.ReadNextRecord())
                {
                    MachName = Act_fileName;
                        MachName= MachName.Replace("63511098", "");
                        MachName = MachName.Replace("43513019", "");

                        Act_fileName = MachName;
                        //Program Name
                        ProgNo = csv[1];
                    if (Act_fileName.StartsWith("H") || Act_fileName.StartsWith("FBD") && (!Act_fileName.StartsWith("FBD3-1253NT")))
                    {

                        DateTime Date1 = new DateTime();
                        DateTime Date2 = new DateTime();

                        //Process start time
                        procstrt = csv[3];
                        Date1 = DateTime.Parse(procstrt);

                        //Process end time
                        procend = csv[4];
                        Date2 = DateTime.Parse(procend);

                        //Good Qty
                        goodqty = csv[5];

                        //Defective Qty
                        defqty = csv[6];
                        //Process Time
                        ProcTime = Date2 - Date1;

                        bendsteps = csv[10];

                        //Defective Qty
                        shots = csv[11];

                        int def_qty = Convert.ToInt16(defqty);
                        int good_qty = Convert.ToInt16(goodqty);
                        double ProcTimeSecs = ProcTime.TotalSeconds;
                        string str_dval = csv[4];
                        str_dval = str_dval.Replace("/", "-");

                        if (str_dval.StartsWith(cur_date))
                        {
                            string CmdText = "INSERT IGNORE INTO yoshimi_csvquickdata VALUES(@idcsv,@machname,@progno,@procstrt,@procend,@goodqty,@defqty,@proctimesecs,@bendsteps,@shots)";
                            MySqlCommand cmd = new MySqlCommand(CmdText, mcon);
                            cmd.Parameters.AddWithValue("@idcsv", MachName + "-" + Date1.Hour + "-" + Date1.Minute + "-" + Date1.Second);
                            cmd.Parameters.AddWithValue("@machname", MachName);
                            cmd.Parameters.AddWithValue("@progno", ProgNo);
                            cmd.Parameters.AddWithValue("@procstrt", Date1);
                            cmd.Parameters.AddWithValue("@procend", Date2);
                            cmd.Parameters.AddWithValue("@goodqty", goodqty);
                            cmd.Parameters.AddWithValue("@defqty", defqty);
                            cmd.Parameters.AddWithValue("@proctimesecs", ProcTimeSecs);
                            cmd.Parameters.AddWithValue("@bendsteps", bendsteps);
                            cmd.Parameters.AddWithValue("@shots", shots);

                            int res_val = cmd.ExecuteNonQuery();
                                /*if (res_val == 1 && good_qty > 0)
                                {
                                    if (String.IsNullOrEmpty(ProgNo_old))
                                    {
                                        starttime = procstrt.Replace("/", "-");
                                        endtime = procend.Replace("/", "-");
                                        ProgNo_old = ProgNo;
                                        good_qty_val = good_qty;
                                        update_flag = 1;
                                    }
                                    else if (ProgNo_old.Equals(ProgNo))
                                    {
                                        starttime = procstrt.Replace("/", "-");
                                        ProgNo_old = ProgNo;
                                        good_qty_val = good_qty_val + good_qty;
                                        update_flag = 1;
                                    }
                                    else
                                    {
                                        Program pg = new Program();
                                        update_flag = 0;
                                        pg.update_GPN(ProgNo_old, "12022", 2, "" + good_qty_val, "2705", false, "-1", starttime, endtime, cur_date);
                                        starttime = procstrt.Replace("/", "-");
                                        endtime = procend.Replace("/", "-");
                                        ProgNo_old = ProgNo;
                                        good_qty_val = good_qty;
                                    }
                                }*/
                            }
                            else
                        {
                           // if (update_flag == 1)
                            //{
                             //   Program pg = new Program();
                            //    pg.update_GPN(ProgNo, "12022", 2, "" + good_qty_val, "2705", false, "-1", starttime, endtime, cur_date);
                            //    update_flag = 0;
                           // }
                        }
                        counter++;
                    }
                    else
                    {
                        //Machine Name

                        DateTime Date1 = new DateTime();
                        DateTime Date2 = new DateTime();

                        //Process start time
                        procstrt = csv[4];
                        Date1 = DateTime.Parse(procstrt);

                        //Process end time
                        procend = csv[5];
                        Date2 = DateTime.Parse(procend);

                        //Good Qty
                        goodqty = csv[7];

                        //Defective Qty
                        defqty = csv[8];
                        //Process Time
                        ProcTime = Date2 - Date1;

                        int def_qty = Convert.ToInt16(defqty);
                        int good_qty = Convert.ToInt16(goodqty);
                        double ProcTimeSecs = ProcTime.TotalSeconds;
                        string str_dval = csv[5];
                        str_dval = str_dval.Replace("/", "-");
                      
                        if (str_dval.StartsWith(cur_date))
                        {
                            string CmdText = "INSERT IGNORE INTO yoshimi_csvquickdata VALUES(@idcsv,@machname,@progno,@procstrt,@procend,@goodqty,@defqty,@proctimesecs,@bendsteps,@shots)";
                            MySqlCommand cmd = new MySqlCommand(CmdText, mcon);
                            cmd.Parameters.AddWithValue("@idcsv", MachName + "-" + Date1.Hour + "-" + Date1.Minute + "-" + Date1.Second);
                            cmd.Parameters.AddWithValue("@machname", MachName);
                            cmd.Parameters.AddWithValue("@progno", ProgNo);
                            cmd.Parameters.AddWithValue("@procstrt", Date1);
                            cmd.Parameters.AddWithValue("@procend", Date2);
                            cmd.Parameters.AddWithValue("@goodqty", goodqty);
                            cmd.Parameters.AddWithValue("@defqty", defqty);
                            cmd.Parameters.AddWithValue("@proctimesecs", ProcTimeSecs);
                            cmd.Parameters.AddWithValue("@bendsteps", 0);
                            cmd.Parameters.AddWithValue("@shots", 0);

                            int res_val = cmd.ExecuteNonQuery();
                            if (def_qty == 0)
                            {
                                updateYOSHIMIpgminfo(ProgNo, procstrt, procend, mcon2);
                            }
                           
                        }
                        counter++;
                    }
                }
            }
        }

        catch (Exception e)
        {
            using (StreamWriter w = File.AppendText("log.txt"))
            {
                Log("Error:" + MachName + e, w);
            }
        }
    }

  
    public static void Log(string logMessage, TextWriter w)
    {
        w.Write("\r\nLog Entry : ");
        w.WriteLine($"{DateTime.Now.ToLongTimeString()} {DateTime.Now.ToLongDateString()}");
        w.WriteLine("  :");
        w.WriteLine($"  :{logMessage}");
        w.WriteLine("-------------------------------");
    }

    public static void DumpLog(StreamReader r)
    {
        string line;
        while ((line = r.ReadLine()) != null)
        {
            Console.WriteLine(line);
        }
    }
}
}
